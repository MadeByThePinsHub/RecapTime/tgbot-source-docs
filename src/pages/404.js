import React from 'react'
import Layout from '../components/Layout'

// Instead of defining 'NotFoundPage', Andrei Jiroh opts to use 'DekuNotFound' isntead.
const DekuNotFound = () => (
  <Layout>
    <h1>That didn't found on our docs.</h1>
    <p></p>
  </Layout>
)

export default DekuNotFound
